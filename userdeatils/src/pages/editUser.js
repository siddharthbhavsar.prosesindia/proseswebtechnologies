import { useEffect, useState } from 'react'
import { useLocation, useNavigate, Link} from 'react-router-dom'
import { toast } from 'react-toastify'
import config from '../config'
import axios from 'axios'



const EditUser = () => {
  const [mobileNo,setMobileNo]=useState('')
  const [email,setEmail]=useState('')
  const [address,setAddress]=useState('')
  
  const location = useLocation()
  const navigate=useNavigate

  useEffect(() => {
    const { homeUser } = location.state
    save(homeUser)
  }, [])

    const save=(username)=>{
      if(mobileNo.length===0){
        toast.error('Please Enter Mobile Number')
      }else if(email.length===0){
        toast.error('Please Enter Email')
      }else if(address.length===0){
        toast.error('Please Enter Address')
      }else{
        axios.put(config.serverURL + '/editUser' + username,{
          mobileNo,email,address}).then((response)=>{
          const result = response.data
          if (result['status'] === 'error') {
            toast.error('Error')
          } else {
            sessionStorage['token'] = result['data']['token']
            sessionStorage['username'] = result['data']['name']

            toast.success('Succesfully edit user done')
            navigate('/home')

          }

        }).catch((error) => {
          console.log('error')
          console.log(error)
        })
      }
    
  }
  return (
    <div style={{ marginTop: 100 }}>
      <div style={styles.container}>
        <div className='mb-3'>
          <label>Mobile Number</label>
          <input onChange={(event)=>{
            setMobileNo(event.target.value)
          }} className='form-control' type='text' />
        </div>

        <div className='mb-3'>
          <label>Email</label>
          <input onChange={(event)=>{
            setEmail(event.target.value)
          }} className='form-control' type='text' />
        </div>
        <div className='mb-3'>
          <label>Address</label>
          <input onChange={(event)=>{
            setAddress(event.target.value)
          }} className='form-control' type='email' />
        </div>

        
        <div className='mb-3' style={{ marginTop: 40 }}>
          
          <button onClick={save} style={styles.saveButton}>Save</button>
        </div>
      </div>
    </div>
  )
}
const styles = {
    container: {
      width: 400,
      height: 450,
      padding: 20,
      position: 'relative',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      margin: 'auto',
      borderColor: '#db0f62',
      borderRadius: 10,
      broderWidth: 1,
      borderStyle: 'solid',
      boxShadow: '1px 1px 20px 5px #C9C9C9',
    },
    saveButton: {
      position: 'relative',
      width: '100%',
      height: 40,
      backgroundColor: '#db0f62',
      color: 'white',
      borderRadius: 5,
      border: 'none',
      marginTop: 10,
    },
  }
  
export default EditUser
