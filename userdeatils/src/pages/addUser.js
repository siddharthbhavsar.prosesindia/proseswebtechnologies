import { useState } from 'react'
import { useNavigate} from 'react-router-dom'
import { toast } from 'react-toastify'

import axios from 'axios'
import config from '../config'



const AddUser = () => {
  const [username,setUsername]=useState('')
  const [mobileNo,setMobileNo]=useState('')
  const [email,setEmail]=useState('')
  const [address,setAddress]=useState('')

  const navigate=useNavigate

    const adduser=()=>{
        
      if(username.length===0){
        toast.error('Please Enter Username')
      }else if(mobileNo.length===0){
        toast.error('Please Enter Mobile Number')
      }else if(email.length===0){
        toast.error('Please Enter Email')
      }else if(address.length===0){
        toast.error('Please Enter Address')
      }else{
        axios.post(config.serverURL + '/addUser',{
          username,mobileNo,email,address}).then((response)=>{
          const result = response.data
          if (result['status'] === 'error') {
            toast.error('Error') 
          } else {
            sessionStorage['token'] = result['data']['token']
            sessionStorage['username'] = result['data']['name']

            toast.success('User succesfully added!!!')
            navigate('/home')
          }

        }).catch((error) => {
          console.log('error')
          console.log(error)
        })
      }
    
  }
  return (
    <div style={{ marginTop: 100 }}>
      <div style={styles.container}>
        
        <div className='mb-3'>
          <label>Username</label>
          <input onChange={(event)=>{
            setUsername(event.target.value)
          }} className='form-control' type='tel' />
        </div>
        <div className='mb-3'>
          <label>Mobile Number</label>
          <input onChange={(event)=>{
            setMobileNo(event.target.value)
          }} className='form-control' type='text' />
        </div>
        <div className='mb-3'>
          <label>Email</label>
          <input onChange={(event)=>{
            setEmail(event.target.value)
          }} className='form-control' type='text' />
        </div>
        <div className='mb-3'>
          <label>Address</label>
          <input onChange={(event)=>{
            setAddress(event.target.value)
          }} className='form-control' type='text' />
        </div>

        <div className='mb-3' style={{ marginTop: 40 }}>
          <button onClick={adduser} style={styles.addUserButton}>Add User</button>
        </div>
      </div>
    </div>
  )
}
const styles = {
    container: {
      width: 400,
      height: 550,
      padding: 20,
      position: 'relative',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
      margin: 'auto',
      borderColor: '#00ffff',
      borderRadius: 10,
      broderWidth: 1,
      borderStyle: 'solid',
      boxShadow: '1px 1px 20px 5px #C9C9C9',
    },
    addUserButton: {
      position: 'relative',
      width: '100%',
      height: 40,
      backgroundColor: '#00ffff',
      color: 'white',
      borderRadius: 5,
      border: 'none',
      marginTop: 10,
    },
  }

export default AddUser