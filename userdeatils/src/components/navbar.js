import { Link } from 'react-router-dom'

const Navbar = () => {
  return (
    <nav
      style={{ backgroundColor: '#00ffff' }}
      className='navbar  navbar-expand-sm bg-info navbar-dark'>
      <div className='container-fluid'>
        <Link className='navbar-brand' to='/home'>
          Proses Assignment
        </Link>

        <div className='collapse navbar-collapse'>
          <ul className='navbar-nav me-auto mb-2 mb-lg-0'>
            <li className='nav-item'>
              <Link className='nav-link active' aria-current='page' to='/home'>
                Home
              </Link>
            </li>
            <li className='nav-item'>
              <Link
                className='nav-link active'
                aria-current='page'
                to='/adduser'>
                Add User
              </Link>
            </li>
            
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Navbar
