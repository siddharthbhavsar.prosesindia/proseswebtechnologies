const express = require('express')
const cors = require('cors')

const app = express()
app.use(express.json())
app.use(cors())

const userRouter = require('./user')

app.use('/user', userRouter)

app.listen(4000, '0.0.0.0', () => {
  console.log('Server started on port 4000')
})
