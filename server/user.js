const express = require('express')
const db=require('./db')
const utils=require('./utils')

const router=express.Router()

//view user's
router.get('/viewUser',(request,response)=>{
  
  const statement=
  `SELECT username, mobileNo, email, address FROM user`

  db.pool.query(statement,(error,viewProuct)=>{
      response.send(utils.createResult(error,viewProuct))
  })
})

//add user
router.post('/addUser', (request, response) => {
    const { username, mobileNo, email, address } = request.body
    

    const statement = `
      INSERT INTO user
          (username, mobileNo, email, address)
      VALUES (?, ?, ?, ?)
    `
    db.pool.query(
      statement,
      [username, mobileNo, email, address],
      (error, result) => {
        response.send(utils.createResult(error, result))
      }
    )
  })

//edit user 
router.put('/editUser/:username', (request, response) => {
    const{username}=request.params
    const { mobileNo, email, address } = request.body

    const statement = `
      UPDATE user set mobileNo=?,email=?,address=?
      where username=?
    `
    db.pool.query(
      statement,
      [ mobileNo, email, address, username ],
      (error, result) => {
        response.send(utils.createResult(error, result))
      }
    )
  })

//delete user 
router.delete('/deleteUser/:username',(request,response)=>{
  const{username}=request.params
  const statement=
  `DELETE FROM user WHERE username=?`

  db.pool.query(statement,
    [username],
    (error,result)=>{
      response.send(utils.createResult(error,result))
  })
})

  module.exports = router